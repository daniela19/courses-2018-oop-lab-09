package it.unibo.oop.lab.reactivegui03;

import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public final class ConcurrentGUI extends JFrame {
    private static final long serialVersionUID = 1L;
    private static final double WIDTH_PERC = 0.2;
    private static final double HEIGHT_PERC = 0.1;
    private final JLabel display = new JLabel();
    private final JButton stop = new JButton("stop");
    private final JButton up = new JButton("up");
    private final JButton down = new JButton("down");

    public ConcurrentGUI() {
        super();
        final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        this.setSize((int) (screenSize.getWidth() * WIDTH_PERC), (int) (screenSize.getHeight() * HEIGHT_PERC));
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        final JPanel canvas = new JPanel();
        this.getContentPane().add(canvas);
        canvas.add(display);
        canvas.add(up);
        canvas.add(down);
        canvas.add(stop);
        this.setVisible(true);
        final Agent agent = new Agent();
        final Agent2 agent2 = new Agent2();
        new Thread(agent2).start();

        up.addActionListener(l -> {
            agent.up();
        });

        down.addActionListener(l -> {
            agent.down();
        });

        stop.addActionListener(l -> {
            agent.stop();
        });
    }

    private class Agent implements Runnable {
        private volatile boolean ascending = true;
        private volatile boolean stop;
        private int counter;

        @Override
        public void run() {
            while (!this.stop) {
                try {
                    SwingUtilities
                            .invokeAndWait(() -> ConcurrentGUI.this.display.setText(Integer.toString(this.counter)));
                    this.counter += ascending ? 1 : -1;
                    Thread.sleep(100);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                // TODO Auto-generated method stub

            }

        }

        public void up() {
            this.up = true;
        }

        public void down() {
            this.up = false;
        }

        public void stop() {
            this.stop = true;
            ConcurrentGUI.this.up.setEnabled(false);
            ConcurrentGUI.this.down.setEnabled(false);
            ConcurrentGUI.this.stop.setEnabled(false);
        }
    }

    private class Agent2 implements Runnable {
        final Agent agent = new Agent();

        @Override
        public void run() {
            try {
                new Thread(agent).join(10000);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        }

    }
}
